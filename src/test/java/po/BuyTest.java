package po;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class BuyTest {
  @BeforeClass
  void setUp() {
    Configuration.baseUrl = "http://automationpractice.com";
    Configuration.startMaximized = true;
    Configuration.holdBrowserOpen = false;
  }

  @AfterClass
  void tearDown() {
    Selenide.closeWindow();
  }

  @Test
  public void buy() {
    MainPage page = open("/index.php", MainPage.class);
    CartPage cart = page
        .signIn()
        .login("w@f.s", "111111")
        .goToMainPage()
        .addToCart()
        .acceptSummary()
        .acceptAddress()
        .acceptShipping()
        .payByBankWire();
    $x(cart.getOrderConfirmed()).shouldBe(Condition.exist);

  }

}
