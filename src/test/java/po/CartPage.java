package po;


import lombok.Getter;

import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

@Getter
public class CartPage {
  private final String orderConfirmed = "//*[text()='Your order on My Store is complete.']";
  private final String acceptSummaryButton = "#center_column a.standard-checkout";
  private final String uniformButton = "#uniform-cgv";
  private final String acceptShippingButton = "#form button[type=submit]";
  private final String payBankWireButton = "#HOOK_PAYMENT a.bankwire";
  private final String confirmOrderButton = "//*[text()='I confirm my order']";

  public CartPage acceptSummary() {
    $(acceptSummaryButton).scrollTo().click();
    return this;
  }

  public CartPage acceptAddress() {
    $(byName("processAddress")).scrollTo().click();
    return this;
  }

  public CartPage acceptShipping() {
    $(uniformButton).click();
    $(acceptShippingButton).scrollTo().click();
    return this;
  }

  public CartPage payByBankWire() {
    $(payBankWireButton).click();
    $x(confirmOrderButton).click();
    return this;
  }

}
