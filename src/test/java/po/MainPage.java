package po;

import lombok.Getter;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

@Getter
public class MainPage {
  private final String item = ".tab-content > ul > li";
  private final String currentItem = "[data-id-product='1']";
  private final String buyButton = ".button.button-medium span";

  public SignInPage signIn() {
    $(byText("Sign in")).click();
    return page(SignInPage.class);
  }

  public CartPage addToCart() {
    $(item).hover();
    $(currentItem).click();
    $(buyButton).click();
    return page(CartPage.class);
  }
}