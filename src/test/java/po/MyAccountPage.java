package po;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class MyAccountPage {


  public PersonalInfoPage clickPersonalInfo() {
    $("#center_column a[title=\"Information\"]").click();
    return page(PersonalInfoPage.class);
  }

  public MainPage goToMainPage() {
    $("#header_logo").click();
    return page(MainPage.class);
  }
}