package po;

import lombok.Getter;

@Getter
public class PersonalInfoPage {
  private final String firstName = "#firstname";
  private final String lastName = "#lastname";
}
