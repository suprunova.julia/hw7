package po;


import lombok.Getter;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

@Getter
public class RegisterPage {
  private final String firstName = "#customer_firstname";
  private final String lastName = "#customer_lastname";
  private final String genderMs = "#id_gender2";
  private final String password = "#passwd";
  private final String address = "#address1";
  private final String city = "#city";
  private final String state = "#id_state";
  private final String postcode = "#postcode";
  private final String mobilePhone = "#phone_mobile";
  private final String alias = "#alias";
  private final String submitButton = "#submitAccount";

  public RegisterPage setGenderMs() {
    $(genderMs).selectRadio("2");
    return this;
  }

  public RegisterPage setFirstName(String firstName) {
    $(this.firstName).sendKeys(firstName);
    return this;
  }

  public RegisterPage setLastName(String lastName) {
    $(this.lastName).sendKeys(lastName);
    return this;
  }

  public RegisterPage setPassword(String password) {
    $(this.password).sendKeys(password);
    return this;
  }

  public RegisterPage setAddress(String address) {
    $(this.address).sendKeys(address);
    return this;
  }

  public RegisterPage setCity(String city) {
    $(this.city).sendKeys(city);
    return this;
  }

  public RegisterPage setState(String state) {
    $(this.state).selectOptionByValue(state);
    return this;
  }

  public RegisterPage setPostCode(String postcode) {
    $(this.postcode).sendKeys(postcode);
    return this;
  }

  public RegisterPage setMobile(String mobilePhone) {
    $(this.mobilePhone).sendKeys(mobilePhone);
    return this;
  }

  public RegisterPage setAlias(String alias) {
    $(this.alias).setValue("").sendKeys(alias);
    return this;
  }

  public MyAccountPage submitAccount() {
    $(this.submitButton).click();
    return page(MyAccountPage.class);
  }
}