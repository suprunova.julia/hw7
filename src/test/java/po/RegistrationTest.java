package po;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.ThreadLocalRandom;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class RegistrationTest {
  @BeforeClass
  void setUp() {
    Configuration.baseUrl = "http://automationpractice.com";
    Configuration.startMaximized = true;
    Configuration.holdBrowserOpen = false;
  }

  @AfterClass
  void tearDown() {
    Selenide.closeWindow();
  }

  @Test
  public void registration() {
    MainPage page = open("/index.php", MainPage.class);
    SignInPage signInPage = page.signIn();
    String email = "julia1" + ThreadLocalRandom.current().nextInt() + "@gmail.com";
    signInPage.fillEmailToCreateAcc(email);
    $(signInPage.getEmail()).shouldHave(attribute("value", email));
    RegisterPage registerPage = signInPage.submitCreateAccount();
    $(registerPage.getFirstName()).shouldBe(Condition.visible);
    $(registerPage.getLastName()).shouldBe(Condition.visible);
    PersonalInfoPage personalInfoPage = registerPage
        .setGenderMs()
        .setFirstName("Julia")
        .setLastName("S")
        .setPassword("123456qq")
        .setAddress("Kyiv,Holosiivska vul,49k2")
        .setCity("Kyiv")
        .setState("2")
        .setPostCode("07101")
        .setMobile("+380989667062")
        .setAlias("address")
        .submitAccount()
        .clickPersonalInfo();
    $(personalInfoPage.getFirstName()).shouldHave(attribute("value", "Julia"));
    $(personalInfoPage.getLastName()).shouldHave(attribute("value", "S"));
  }

}
