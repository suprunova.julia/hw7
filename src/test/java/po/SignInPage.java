package po;

import lombok.Getter;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

@Getter
public class SignInPage {
  private final String email = "[data-validate=isEmail]";
  private final String createButton = "#SubmitCreate";
  private final String loginEmail = "#email";
  private final String password = "#passwd";
  private final String loginButton = "#SubmitLogin";

  public void fillEmailToCreateAcc(String email) {
    $(this.email).sendKeys(email);
  }

  public RegisterPage submitCreateAccount() {
    $(createButton).click();
    return page(RegisterPage.class);
  }

  public MyAccountPage login(String loginEmail, String password) {
    $(this.loginEmail).sendKeys(loginEmail);
    $(this.password).sendKeys(password);
    $(this.loginButton).click();
    return page(MyAccountPage.class);
  }

}